unit UCadAluno;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Data.DB, Vcl.Grids, Vcl.DBGrids;

type
  TForm2 = class(TForm)
    zz: TDBGrid;
    DtsCadAluno: TDataSource;
    PnlDados: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    insertNome: TEdit;
    insertIdade: TEdit;
    insertCPF: TEdit;
    insertRG: TEdit;
    BtnSalvar: TBitBtn;
    BtnExcluir: TBitBtn;
    Panel1: TPanel;
    procedure BtnSalvarClick(Sender: TObject);
  private

  procedure LimpaCampo();
    
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

{ TForm2 }

procedure TForm2.BtnSalvarClick(Sender: TObject);
begin

     LimpaCampo();

end;

procedure TForm2.LimpaCampo;

begin

  insertNome.Text:= '';
  insertIdade.Text:= '';
  insertCPF.Text:= '';
  insertRG.Text:= '';

end;

end.
