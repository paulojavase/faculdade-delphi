object DataModule1: TDataModule1
  OldCreateOrder = False
  Height = 275
  Width = 492
  object Conexao: TZConnection
    ControlsCodePage = cCP_UTF16
    Connected = True
    HostName = 'localhost'
    Port = 0
    Database = 'faculdade'
    User = 'root'
    Password = 'root'
    Protocol = 'mysql'
    Left = 104
    Top = 32
  end
  object qryCadAluno: TZQuery
    Connection = Conexao
    Filtered = True
    Active = True
    SQL.Strings = (
      'select * from aluno')
    Params = <>
    Left = 272
    Top = 32
    object qryCadAlunoRA: TSmallintField
      Alignment = taCenter
      DisplayWidth = 5
      FieldName = 'RA'
      Required = True
    end
    object qryCadAlunoNOME: TWideStringField
      Alignment = taCenter
      DisplayWidth = 17
      FieldName = 'NOME'
      Size = 15
    end
    object qryCadAlunoId_Contato: TSmallintField
      Alignment = taCenter
      FieldName = 'Id_Contato'
      Visible = False
    end
    object qryCadAlunoidade: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'IDADE'
      DisplayWidth = 11
      FieldName = 'idade'
    end
    object qryCadAlunocpf: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'CPF'
      DisplayWidth = 12
      FieldName = 'cpf'
      Size = 11
    end
    object qryCadAlunorg: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'RG'
      DisplayWidth = 11
      FieldName = 'rg'
      Size = 10
    end
  end
end
