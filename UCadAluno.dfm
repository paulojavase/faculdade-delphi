object Form2: TForm2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Cad Aluno'
  ClientHeight = 511
  ClientWidth = 677
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object zz: TDBGrid
    Left = 2
    Top = 207
    Width = 677
    Height = 298
    Align = alCustom
    DataSource = DtsCadAluno
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Sitka Small'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RA'
        Title.Alignment = taCenter
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -15
        Title.Font.Name = 'Sitka Small'
        Title.Font.Style = [fsBold, fsItalic]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Alignment = taCenter
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -15
        Title.Font.Name = 'Sitka Small'
        Title.Font.Style = [fsBold, fsItalic]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'idade'
        Title.Alignment = taCenter
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -15
        Title.Font.Name = 'Sitka Small'
        Title.Font.Style = [fsBold, fsItalic]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cpf'
        Title.Alignment = taCenter
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -15
        Title.Font.Name = 'Sitka Small'
        Title.Font.Style = [fsBold, fsItalic]
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'rg'
        Title.Alignment = taCenter
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -15
        Title.Font.Name = 'Sitka Small'
        Title.Font.Style = [fsBold, fsItalic]
        Visible = True
      end>
  end
  object PnlDados: TPanel
    Left = -1
    Top = 79
    Width = 680
    Height = 122
    Align = alCustom
    TabOrder = 1
    object Label1: TLabel
      Left = 17
      Top = 8
      Width = 51
      Height = 25
      Caption = 'NOME:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Be True To Your School'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 17
      Top = 48
      Width = 54
      Height = 25
      Caption = 'IDADE:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Be True To Your School'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 185
      Top = 47
      Width = 36
      Height = 25
      Caption = 'CPF:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Be True To Your School'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 450
      Top = 47
      Width = 24
      Height = 25
      Caption = 'RG:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Be True To Your School'
      Font.Style = []
      ParentFont = False
    end
    object insertNome: TEdit
      Left = 73
      Top = 8
      Width = 584
      Height = 21
      TabOrder = 0
    end
    object insertIdade: TEdit
      Left = 81
      Top = 48
      Width = 53
      Height = 21
      Alignment = taCenter
      TabOrder = 1
    end
    object insertCPF: TEdit
      Left = 225
      Top = 47
      Width = 155
      Height = 21
      Alignment = taCenter
      TabOrder = 2
    end
    object insertRG: TEdit
      Left = 480
      Top = 47
      Width = 177
      Height = 21
      Alignment = taCenter
      TabOrder = 3
    end
    object BtnSalvar: TBitBtn
      Left = 480
      Top = 87
      Width = 75
      Height = 25
      Caption = 'SALVAR'
      TabOrder = 4
      Visible = False
      OnClick = BtnSalvarClick
    end
    object BtnExcluir: TBitBtn
      Left = 582
      Top = 87
      Width = 75
      Height = 25
      Caption = 'EXCLUIR'
      TabOrder = 5
    end
  end
  object Panel1: TPanel
    Left = -18
    Top = 0
    Width = 697
    Height = 73
    Align = alCustom
    Caption = 'CADASTRAR ALUNO'
    Color = 42495
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -64
    Font.Name = 'Be True To Your School'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
  end
  object DtsCadAluno: TDataSource
    DataSet = DataModule1.qryCadAluno
    Left = 530
    Top = 328
  end
end
