unit UPessoa;

interface

type

TPessoa=class

private

FNome: String;
FIdade: String;
FCPF: String;
FRg: String;
    function getNome: String;
    procedure setNome(const Value: String);
    function getIdade: String;
    procedure SetIdade(const Value: String);
    function getCpf: String;
    procedure setCpf(const Value: String);
    function getRg: String;
    procedure setRg(const Value: String);

public 

property nome: String read getNome write setNome; 
property idade: String read getIdade write SetIdade;
property cpf: String read getCpf write setCpf;
property Rg: String read getRg write setRg;

end;

implementation

{ TPessoa }

function TPessoa.getCpf: String;
begin
result:= FCpf;
end;

function TPessoa.getIdade: String;
begin
result:= FIdade;
end;

function TPessoa.getNome: String;
begin
result:= FNome;
end;

function TPessoa.getRg: String;
begin
result:= FRg;
end;

procedure TPessoa.setCpf(const Value: String);
begin
FCpf:= Value;
end;

procedure TPessoa.SetIdade(const Value: String);
begin
FIdade:= Value;
end;

procedure TPessoa.setNome(const Value: String);
begin
Fnome:= Value;
end;

procedure TPessoa.setRg(const Value: String);
begin
FRg:= Value;
end;

end.
