program Faculdade;

uses
  Vcl.Forms,
  UCadastroAluno in 'UCadastroAluno.pas' {Form1},
  UPessoa in 'UPessoa.pas',
  UDTM in 'UDTM.pas' {DataModule1: TDataModule},
  UAluno in 'UAluno.pas',
  UCadAluno in 'UCadAluno.pas' {Form2};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
